package resolver

import (
	"context"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var g map[string]interface{}

type DB struct {
	mainDB *gorm.DB
	backDB *gorm.DB
}

type Association struct {
	mainAssociation *gorm.Association
	backAssociation *gorm.Association
}

// 主主模式初始化，当db1执行异常时，使用db2重试一次
func Init(dsn1 string, dsn2 string, opts ...gorm.Option) *DB {
	db1, err := gorm.Open(mysql.Open(dsn1), opts...)
	if err != nil {
		panic(err)
	}
	db2, err := gorm.Open(mysql.Open(dsn2), opts...)
	if err != nil {
		panic(err)
	}

	return &DB{mainDB: db1, backDB: db2}
}

/*
读写分离模式+主主重试模式,
db1需使用https://github.com/go-gorm/dbresolver库指定读写分离规则
db2与db1的写库是主主同步模式
*/
func InitRWMode(db1 *gorm.DB, db2 *gorm.DB) *DB {
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) WithContext(ctx context.Context) *DB {
	db1 := db.mainDB.WithContext(ctx)
	db2 := db.backDB.WithContext(ctx)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Create(value interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Create(value)
	if tx.Error != nil {
		tx = db.backDB.Create(value)
	}
	return tx
}

func (db *DB) Save(value interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Save(value)
	if tx.Error != nil {
		tx = db.backDB.Save(value)
	}
	return tx
}

func (db *DB) First(dest interface{}, conds ...interface{}) error {
	tx := db.mainDB.First(dest, conds...)
	if tx.Error != nil && tx.Error != gorm.ErrRecordNotFound {
		tx = db.backDB.First(dest, conds...)
	}
	if tx.Error == gorm.ErrRecordNotFound {
		return nil
	}
	return tx.Error
}

func (db *DB) FirstOrCreate(dest interface{}, conds ...interface{}) error {
	tx := db.mainDB.FirstOrCreate(dest, conds...)
	if tx.Error != nil && tx.Error != gorm.ErrRecordNotFound {
		tx = db.backDB.FirstOrCreate(dest, conds...)
	}
	return tx.Error
}

func (db *DB) Last(dest interface{}, conds ...interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Last(dest, conds...)
	if tx.Error != nil {
		tx = db.backDB.Last(dest, conds...)
	}
	if tx.Error == gorm.ErrRecordNotFound {
		return nil
	}
	return tx
}

func (db *DB) AutoMigrate(dst ...interface{}) error {
	err1 := db.mainDB.AutoMigrate(dst...)
	err2 := db.backDB.AutoMigrate(dst...)
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return nil
}

func (db *DB) Model(value interface{}) *DB {
	db1 := db.mainDB.Model(value)
	db2 := db.backDB.Model(value)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Table(name string, args ...interface{}) *DB {
	db1 := db.mainDB.Table(name, args...)
	db2 := db.backDB.Table(name, args...)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Raw(sql string, values ...interface{}) *DB {
	db1 := db.mainDB.Raw(sql, values...)
	db2 := db.backDB.Raw(sql, values...)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Where(query interface{}, args ...interface{}) *DB {
	db1 := db.mainDB.Where(query, args...)
	db2 := db.backDB.Where(query, args...)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Limit(limit int) *DB {
	db1 := db.mainDB.Limit(limit)
	db2 := db.backDB.Limit(limit)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Order(value interface{}) *DB {
	db1 := db.mainDB.Order(value)
	db2 := db.backDB.Order(value)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Preload(query string, args ...interface{}) *DB {
	db1 := db.mainDB.Preload(query, args...)
	db2 := db.backDB.Preload(query, args...)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Offset(offset int) *DB {
	db1 := db.mainDB.Offset(offset)
	db2 := db.backDB.Offset(offset)
	return &DB{mainDB: db1, backDB: db2}
}

func (db *DB) Scan(dest interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Scan(dest)
	if tx.Error != nil {
		tx = db.backDB.Scan(dest)
	}
	return tx
}

func (db *DB) Update(column string, value interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Update(column, value)
	if tx.Error != nil {
		tx = db.backDB.Update(column, value)
	}
	return tx
}

func (db *DB) Updates(value interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Updates(value)
	if tx.Error != nil {
		tx = db.backDB.Updates(value)
	}
	return tx
}

func (db *DB) UpdateColumn(column string, value interface{}) (tx *gorm.DB) {
	tx = db.mainDB.UpdateColumn(column, value)
	if tx.Error != nil {
		tx = db.backDB.UpdateColumn(column, value)
	}
	return tx
}

func (db *DB) UpdateColumns(values interface{}) (tx *gorm.DB) {
	tx = db.mainDB.UpdateColumns(values)
	if tx.Error != nil {
		tx = db.backDB.UpdateColumns(values)
	}
	return tx
}

func (db *DB) Count(count *int64) (tx *gorm.DB) {
	tx = db.mainDB.Count(count)
	if tx.Error != nil {
		tx = db.backDB.Count(count)
	}
	return tx
}

func (db *DB) Find(value interface{}, conds ...interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Find(value, conds...)
	if tx.Error != nil {
		tx = db.backDB.Find(value, conds...)
	}
	return tx
}

func (db *DB) Delete(value interface{}, conds ...interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Delete(value, conds...)
	if tx.Error != nil {
		tx = db.backDB.Delete(value, conds...)
	}
	return tx
}

func (db *DB) Exec(sql string, values ...interface{}) (tx *gorm.DB) {
	tx = db.mainDB.Exec(sql, values...)
	if tx.Error != nil {
		tx = db.backDB.Exec(sql, values...)
	}
	return tx
}

func (db *DB) Association(column string) *Association {
	ass1 := db.mainDB.Association(column)
	ass2 := db.backDB.Association(column)
	return &Association{mainAssociation: ass1, backAssociation: ass2}
}

func (db *Association) Find(out interface{}, conds ...interface{}) error {
	err := db.mainAssociation.Find(out, conds...)
	if err != nil {
		err = db.backAssociation.Find(out, conds...)
	}
	return err
}

func (db *Association) Delete(values ...interface{}) error {
	err := db.mainAssociation.Delete(values...)
	if err != nil {
		err = db.backAssociation.Delete(values...)
	}
	return err
}

func (db *Association) Append(values ...interface{}) error {
	err := db.mainAssociation.Append(values...)
	if err != nil {
		err = db.backAssociation.Append(values...)
	}
	return err
}

func (db *Association) Clear() error {
	err := db.mainAssociation.Clear()
	if err != nil {
		err = db.backAssociation.Clear()
	}
	return err
}

func (db *Association) Count() int64 {
	count := db.mainAssociation.Count()
	if count < 0 {
		count = db.backAssociation.Count()
	}
	return count
}

func (db *Association) Replace(values ...interface{}) error {
	err := db.mainAssociation.Replace(values...)
	if err != nil {
		err = db.backAssociation.Replace(values...)
	}
	return err
}
