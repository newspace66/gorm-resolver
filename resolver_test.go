package resolver_test

import (
	"encoding/json"
	"fmt"
	"testing"

	resolver "gitee.com/newspace66/gorm-resolver"
)

var dbInstance *resolver.DB

func init() {
	dsn2 := "root:123456@tcp(9.134.116.49:3306)/mini_backend?charset=utf8mb4&parseTime=True&loc=Local&timeout=3s&readTimeout=10s&writeTimeout=10s"
	dsn := "mini_backend:123456@tcp(9.135.231.129:3306)/mini_backend?charset=utf8mb4&parseTime=True&loc=Local&timeout=3s&readTimeout=10s&writeTimeout=10s"
	dbInstance = resolver.Init(dsn, dsn2)
}

type Article struct {
	Id      int
	Userid  int
	Content string
	Enabled int
}

// 定义结构体和表名的规则
func (*Article) TableName() string {
	return "t_article"
}

func TestFind(t *testing.T) {
	var article Article
	result := dbInstance.Find(&article)
	if result.Error != nil {
		fmt.Printf("error: %v\n", result.Error)
		t.Fatalf("got nil, expected an error")
	}
	body, err := json.Marshal(article)
	fmt.Println("Find", string(body[:]), err)
}
